import UniversalSocialauth from './authenticate'
import { Providers, Facebook, Google, Github, Instagram, Bitbucket, Twitter, Vkontakte, Live, Linkedin, Oauth2, Oauth1 } from './providers'


export {
  UniversalSocialauth,
  Providers,
  Facebook,
  Google,
  Github,
  Instagram,
  Bitbucket,
  Twitter,
  Vkontakte,
  Live,
  Linkedin,
  Oauth2,
  Oauth1,
}


export default UniversalSocialauth
